#!/usr/local/bin/python
# coding: utf-8

import re
import sys
import csv
import argparse
import requests

import pandas as pd

from lxml import html, etree

__author__ = 'Stefanie Schneider'
__email__ = 'stefanie.schneider@itg.uni-muenchen.de'
__version__ = '0.1-dev'
__license__ = 'GPLv3'


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='')

    parser.add_argument('-l', '--limit', const=1, nargs='?', default=150, type=int)
    parser.add_argument('-g', '--metadata', const=1, nargs='?', default=True, type=bool)

    args = parser.parse_args()

    return args


def get_names(base_url: str) -> dict:
    response = requests.get(base_url + '/wiki/Bayerische_K%C3%BCche')

    if response.status_code == 200:
        content = html.fromstring(response.content)
        content = content.xpath('//div[@class="mw-parser-output"]')[0]

        valid_elements = False
        valid_pages = {}

        for element in content.xpath('./*'):
            if r'h2' in element.tag:
                valid_elements = True if r'Übersicht' in element.text_content() else False

            if valid_elements and r'ul' in element.tag:
                for li in element.xpath('./li'):
                    name = li.text_content()
                    name = re.sub(r'.*/', '', name)
                    name = re.sub(r'[,–(].*', '', name)

                    href = li.xpath('./a/@href')[0]

                    # exclude missing references
                    if not href.endswith(r'redlink=1'):
                        valid_pages[name.strip()] = href

        return valid_pages


def get_metadata(page_url: str, base_url: str) -> tuple:
    response = requests.get(base_url + page_url)

    if response.status_code == 200:
        content = html.fromstring(response.content)

        categories = content.xpath('//div[@id="mw-normal-catlinks"]/ul/li')
        categories = [category.text_content() for category in categories]

        description = content.xpath('//div[@class="mw-parser-output"]/p')[0]

        etree.strip_elements(description, 'sup', with_tail=False)
        etree.strip_elements(description, 'style', with_tail=False)

        description = ' '.join(description.text_content().split())

        if description.endswith(':'):
            texts = content.xpath('//div[@class="mw-parser-output"]/ul')[0]
            texts = [text.text_content().strip() for text in texts.xpath('./li')]

            description = description + ' ' + '; '.join(texts) + '.'

        return description.strip(), categories


def process() -> None:
    def append_row(data: pd.DataFrame, row: dict) -> pd.DataFrame:
        return data.append(row, ignore_index=True)

    def save_data(data: pd.DataFrame, file: str) -> None:
        data.to_csv(file, index_label='idx', quoting=csv.QUOTE_NONNUMERIC)

    args = parse_args()

    base_url = 'https://de.wikipedia.org'
    valid_pages = get_names(base_url)

    if valid_pages is not None:
        data_items = pd.DataFrame(columns=['description', 'url'])
        data_categories = pd.DataFrame(columns=['value'])

        data_entries = pd.DataFrame(columns=['item_id', 'category_id'])

        for name in list(valid_pages.keys())[:args.limit]:
            description, categories = get_metadata(valid_pages[name], base_url)
            url = base_url + valid_pages[name]

            data_items = append_row(data_items, {
                'description': description, 'url': url
            })

            for category in categories:
                if category not in data_categories.value.tolist():
                    data_categories = append_row(data_categories, {
                        'value': category
                    })

            category_ids = data_categories.value.isin(categories)
            category_ids = data_categories.index[category_ids].tolist()

            item_id = data_items.index.max()

            for category_id in category_ids:
                data_entries = append_row(data_entries, {
                    'item_id': item_id, 'category_id': category_id
                })

        save_data(data_items, r'glossary_items.csv')
        save_data(data_categories, r'glossary_categories.csv')
        save_data(data_entries, r'glossary_entries.csv')


if __name__ == '__main__':
    sys.exit(process())
